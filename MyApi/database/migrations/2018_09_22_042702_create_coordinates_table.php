<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinates', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('token_id')->unsigned();
            $table->integer('value_x')->unsigned();
            $table->integer('value_y')->unsigned();
            $table->integer('screen_height')->unsigned();
            $table->integer('screen_width')->unsigned();
            $table->text('specific_url');
            $table->timestamps();

            $table->foreign('token_id')
                ->references('id')->on('tokens')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('coordinates');
    }
}
