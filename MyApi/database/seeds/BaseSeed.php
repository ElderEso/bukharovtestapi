<?php

use Illuminate\Database\Seeder;

class BaseSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('truncate table users');
        DB::statement('truncate table coordinates');
        DB::statement('truncate table tokens');
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'email' => 'test@test.ru',
            'password' => 'hash',
            'name' => 'test',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'email' => 'test2@test.ru',
            'password' => 'hash2',
            'name' => 'test2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('tokens')->insert([
            'user_id' => 1,
            'url' => 'localhost',
            'token' => 'LOCAL_TEST_TOKEN',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('tokens')->insert([
            'user_id' => 2,
            'url' => 'localhost',
            'token' => 'LOCAL_TEST_TOKEN2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('coordinates')->insert([
            'token_id' => 2,
            'value_x' => 100,
            'value_y' => 100,
            'screen_height' => 1000,
            'screen_width' =>  1000,
            'specific_url' => 'http://localhost/price',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);


        //
    }
}
