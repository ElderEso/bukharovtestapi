// Extract "GET" parameters from a JS include querystring
function getParams(script_name) {
    // Find all script tags
    var scripts = document.getElementsByTagName("script");
    // Look through them trying to find ourselves
    for(var i=0; i<scripts.length; i++) {
        if(scripts[i].src.indexOf("/" + script_name) > -1) {
            // Get an array of key=value strings of params
            var pa = scripts[i].src.split("?").pop().split("&");
            // Split each key=value into array, the construct js object
            var p = {};
            for(var j=0; j<pa.length; j++) {
                var kv = pa[j].split("=");
                p[kv[0]] = kv[1];
            }
            return p;
        }
    }
    return {};
}

window.addEventListener("click", function (evnt) {
    const url = 'http://localhost/api/coordinates';
    let token = getParams('watcher.js')['token'];

    //если токен не указан, тогда выдаем ошибку в консоль
    if(typeof token === 'undefined'){
        console.log('Упс. Кажется вы забыли указать токен при подключении скрипта.');
        console.log('Убедитесь что токен подключен следующим образом: <\script src="http://localhost/js/watcher.js?token=YOUR_TOKEN"\></script>');
        return null;
    }
    console.log(getParams('watcher.js')['token']);
    //вычисляет максимальную ширину экрана
    let scrollHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );
    // для совместимости window.event is not declared
    evnt = evnt || window.event;
    let data = JSON.stringify({
        'token': token,
        'value_x' :  evnt.pageX,
        'value_y' :  evnt.pageY,
        'screen_height' :  scrollHeight,
        'screen_width' :  document.documentElement.clientWidth,
        'specific_url'  :  window.location.href,
    });
    let fetchData = {
        method: 'POST',
        body: data,
        headers: {'Content-Type': 'application/json'},
    };
    //отправляем данные и выводим ответы в консоль
    fetch(url, fetchData)
        .then(function(response) {
            console.log(response);
            return response.json();
        })
        .then(function(responseData) {
            console.log(responseData);
        });
});

