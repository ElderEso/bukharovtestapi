import React from 'react';

let Bar = require("react-chartjs").Bar;

//выводи чартджс функцию типа Бар (смотри выше)
export default function VisitChart(props) {
    let {dataArray} = props;
    //инициализация датасета
    let data = {
        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "#2FA360",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: dataArray
            },

        ]
    };
    return (
        <div>
            <h1>Частота кликов по времени</h1>
            <Bar data={data} width="600" height="250"/>
        </div>
    )
}
