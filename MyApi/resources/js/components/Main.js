import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import VisitChart from './VisitChart';
import HeatmapCanvas from './HeatmapCanvas';

//функция для обновления стейта из под мейн компоненты
function updateCoordinateInfo(tokenCoordinates, token) {
    this.setState({coordinates: tokenCoordinates, token});
}

//отображает первоначальную информацию о токенах в виде таблицы
class Main extends Component {

    constructor() {

        super();
        //Initialize the state in the constructor
        this.state = {
            tokens: [],
            currentToken: null,
            tokenCoordinates: [],
        }
    }
    //до того как смонтиовать компоненту получим данные о токенах из апишки
    componentWillMount() {
        /* fetch API in action */
        fetch('/api/tokens')
            .then(response => {
                return response.json();
            })
            .then(tokens => {
                //Fetched token is stored in the state
                this.setState({tokens});
            });
    }

    //метод для генерации таблицы токенов
    renderTokens() {
        return this.state.tokens.map(token => {
            return (
                <tr key={token.id}>
                    <td>{token.token}</td>
                    <td>{token.url}</td>
                    <td>
                        <button className='btn btn-success' onClick={() => this.handleClick(token)}><span
                            className="material-icons">visibility</span></button>
                    </td>
                </tr>
            );
        })
    }
    //метод для обновления стейта в компоненте КоординатИнфо
    updateChild(tokenCoordinates) {
        updateCoordinateInfo(tokenCoordinates, this.state.currentToken)
    }

    //get all coorinates for this token
    handleClick(token) {
        fetch('api/token/' + token.token + '/coordinates')
            .then(response => {
                return response.json();
            })
            .then(tokenCoordinates => {
                //Fetched coordinates is stored in the state
                this.setState({tokenCoordinates, currentToken: token.token});
                this.updateChild(tokenCoordinates);

            });

    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-12 mt-3">
                        <div className="card">
                            <div className="card-header">
                                Все токены
                            </div>
                            <div className="card-body">
                                <div>
                                    <table className="table table-hover">
                                        <thead>
                                        <tr>
                                            <td>Значение токена</td>
                                            <td>Доменное имя</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.renderTokens()}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col mt-3">
                        <div className="card">
                            <div className="card-header">Информация о координатах</div>
                            <div className="card-body">
                                <CoordinateInfo/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
//компонент для отображения графика и карты
class CoordinateInfo extends Component {

    constructor() {
        super();
        this.state = {
            coordinates: [],
            token: null
        };
        updateCoordinateInfo = updateCoordinateInfo.bind(this)
    }
    //метод который возвращает массив для графика по часам
    //[0 ... 23]
    getVisitArray(tokenCoordinates) {
        let visitArr = [];
        for (let counter = 0; counter <= 23; counter++) {
            visitArr[counter] = 0;
        }
        tokenCoordinates.map(coordinate => {
            let hour = new Date(coordinate.created_at);
            hour = hour.getHours();
            let hoursArray = [];
            for (let counter = 0; counter <= 23; counter++) {
                if (hour === counter) {
                    visitArr[counter] += 1;

                }
            }
        });
        return visitArr;
    }
    //метод который возвращает массив для хитмапа типа
    // [
    // http://localhost/articles:[
    //      1920:[
    //      *** Первый элемент является шириной и высотой экрана текущего сета ***
    //      {1920,1080},
    //      {x: 892, y: 531, value: 5},
    //      {x: 534, y: 123, value: 5},
    //       ...
    //      ],
    //      800:[
    //      {800,600},
    //      {x: 786, y: 531, value: 5},
    //      {x: 534, y: 123, value: 5},
    //       ...
    //      ]
    //]
    getHeatArray(tokenCoordinates) {
        let heatArr = [];
        //идем по полученому из апишки массиву и формируем массив для хитмапа
        tokenCoordinates.map(coordinate => {
            //если нет элементов с таким урлом то вставляем первый ниже иначе
            if (typeof heatArr[coordinate.specific_url] !== 'undefined') {
                //такой УРЛ уже есть в массиве, но если у этого урла нет такой ширины то вставляем элемент ниже
                if (typeof heatArr[coordinate.specific_url][coordinate.screen_width] !== 'undefined') {
                        //иначе просто пушим в [URL][width]
                        heatArr[coordinate.specific_url][coordinate.screen_width].push({x: coordinate.value_x, y: coordinate.value_y, value: 5})
                } else {
                    //здесь вставляем элемент с новой шириной
                    heatArr[coordinate.specific_url][coordinate.screen_width] = [{
                        screen_width: coordinate.screen_width,
                        screen_height: coordinate.screen_height
                    }, {x: coordinate.value_x, y: coordinate.value_y, value: 5}]
                }
            }else{
                //вставляем первый тут
                heatArr[coordinate.specific_url]=[]
                heatArr[coordinate.specific_url][coordinate.screen_width] = [{
                    screen_width: coordinate.screen_width,
                    screen_height: coordinate.screen_height
                }, {x: coordinate.value_x, y: coordinate.value_y, value: 5}]
            }

        });
        return heatArr;
    }

    render() {
        if (this.state.coordinates.length > 0) {
            let visitArr = [];
            let heatArr = [];
            visitArr = this.getVisitArray(this.state.coordinates);
            heatArr = this.getHeatArray(this.state.coordinates);
            return (
                <div>
                    <VisitChart dataArray={visitArr}/>
                    <HeatmapCanvas coordinates={heatArr} class={this.state.token}/>
                </div>
            );
        } else {
            return <span>Нет данных</span>
        }


    }
}

if (document.getElementById('main')) {
    ReactDOM.render(<Main/>, document.getElementById('main'));
}

