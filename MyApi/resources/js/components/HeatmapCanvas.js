import React, {Component} from 'react';
import h337 from "heatmap.js";
import HeatButtonGroup from './HeatButtonGroup';
import HeatSelector from './HeatSelector';
import HeatIntensityInput from './HeatIntensityInput';

//компонент который отображает тепловую карту кликов
export default class HeatmapCanvas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heatmapInstance: null,
            specificUrl: Object.keys(props.coordinates)[0],
            maxDots: 15,
            heatSetIndex: 0,
        }
    }

    //после монтирования компонента накрывает его хитмапом
    componentDidMount() {
        const coordinates = this.props.coordinates[this.state.specificUrl][Object.keys(this.props.coordinates[this.state.specificUrl])[this.state.heatSetIndex]];
        if (document.querySelector('.heatmap')) {
            var heatmapInstance = h337.create({
                container: document.querySelector('.heatmap'),
            });
            this.setState({heatmapInstance: heatmapInstance});
            heatmapInstance.setData({
                max: this.state.maxDots,
                data: coordinates
            });
            heatmapInstance.repaint()
        }
    }

    //на обновление данных компонента обновляет точки на хитмапе, беря старый из стейта
    componentDidUpdate(prevProps) {
        let coordinates = []
        //если ошибка в получнии массива по урл то тупо берем 1 элемент
        try {
            coordinates = this.props.coordinates[this.state.specificUrl][Object.keys(this.props.coordinates[this.state.specificUrl])[this.state.heatSetIndex]];
        } catch (err) {

            coordinates = this.props.coordinates[Object.keys(this.props.coordinates)[0]][Object.keys(this.props.coordinates[Object.keys(this.props.coordinates)[0]])[this.state.heatSetIndex]];
        }
        const prevCoordinates = [[Object.keys(prevProps.coordinates)[this.state.heatSetIndex]]];
        if (typeof coordinates !== 'undefined') {
            if (coordinates.length !== prevCoordinates.length) {

                const coordinatesData = coordinates;
                if (document.querySelector('.heatmap')) {
                    var heatmapInstance = this.state.heatmapInstance

                    heatmapInstance.setData({
                        max: this.state.maxDots,
                        data: coordinatesData
                    });

                }
            }

        }

    }

    //если видим что меняется токен, то возвращаем 0 набор координат, для отображения первого сета координат из нового токена
    componentWillUpdate(prevProps, prevState) {
        if (this.props.class !== prevProps.class) {
            this.setState({heatSetIndex: 0, specificUrl: Object.keys(prevProps.coordinates)[0]})
        }
    }


    //при смене сета удаляем старый хитмап и создаем новый
    changeSet(key) {

        const coordinates = this.props.coordinates[this.state.specificUrl][Object.keys(this.props.coordinates[this.state.specificUrl])[this.state.heatSetIndex]]
        let heatmapInstance = this.state.heatmapInstance
        const canvas = heatmapInstance._renderer.canvas;
        $(canvas).remove()
        heatmapInstance = null
        heatmapInstance = h337.create({
            container: document.querySelector('.heatmap'),
        });
        const index = Object.keys(this.props.coordinates[this.state.specificUrl]).indexOf(key)
        this.setState({heatSetIndex: index, heatmapInstance: heatmapInstance})

    }

    //управление интенсивностью точек
    changeMaxDots(event) {
        this.setState({maxDots: event.target.value})
    }

    //смена сета урлок на другой
    changeSpecificUrl(event) {
        this.setState({specificUrl: event.target.value, heatSetIndex: 0})
    }

    render() {
        let urlArr = [];
        let coordinates = [];
        let specificUrl = null;

        //если ошибка в получении массива по урл то тупо берем 1 элемент
        try {
            urlArr = this.props.coordinates[this.state.specificUrl];
            coordinates = urlArr[Object.keys(urlArr)[this.state.heatSetIndex]];
            specificUrl = this.state.specificUrl;
        } catch (err) {
            urlArr = this.props.coordinates[Object.keys(this.props.coordinates)[0]];
            coordinates = urlArr[Object.keys(urlArr)[this.state.heatSetIndex]];
            specificUrl = Object.keys(this.props.coordinates)[0];
        }

        //если координат нет, то пишем что координат нет
        if (typeof coordinates !== 'undefined') {
            return (
                <div className="demo-wrapper" role={'group'} style={{}}>
                    <h1>Тепловая карта кликов</h1>
                    <div className="row mb-3">
                        <div className="col">
                            {/*блок вывода для выбора разрешения*/}
                            <div className={"btn-group mt-4"}>
                                <HeatButtonGroup changeSet={this.changeSet.bind(this)}
                                                 list={this.props.coordinates[specificUrl]}/>
                            </div>
                        </div>
                        <div className="col">
                            {/*блок вывода инпута для выбора урлки*/}
                            <div>
                                <span className={'text-muted small'}>URLs</span>
                                <HeatSelector changeUrl={this.changeSpecificUrl.bind(this)}
                                              list={this.props.coordinates} default={specificUrl}/>
                            </div>
                        </div>
                        <div className="col-2">
                            {/*блок вывода элемента управления интенсивностью*/}
                            <div>
                                <span className={'text-muted small'}>Интенсивность</span>
                                <HeatIntensityInput default={this.state.maxDots}
                                                    changeMaxDots={this.changeMaxDots.bind(this)}/>
                            </div>
                        </div>
                    </div>
                    {/*непосредственно вывод хитмапа*/}
                    <div className={'heatmap'} id={'heatmap'} style={{
                        height: coordinates[0].screen_height,
                        width: Math.max.apply(Math, Object.keys(this.props.coordinates[specificUrl])),
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        transformOrigin: '0 0',
                        transform: 'scale(0.5)'
                    }}>
                        {/*и контейнер с фреймом внутри*/}
                        <div id="iframeArea" style={{}}>
                            <iframe scrolling={'no'} src={specificUrl} style={{
                                height: coordinates[0].screen_height,
                                width: coordinates[0].screen_width,
                                pointerEvents: 'none'
                            }} allowFullScreen>Iframe text
                            </iframe>
                        </div>
                    </div>
                </div>
            )
        } else {
            return <div>Coordinates are empty</div>
        }
    }

}
