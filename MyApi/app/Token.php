<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = [
        'url',
    ];

    protected $hidden = [
        'value'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coordinates()
    {
        return $this->hasMany(Coordinate::class);
    }

    /**
     * метод для получения токена по имени
     * @param string $token
     * @return mixed
     */
    public static function getTokenByName(string $token)
    {
        return (new Token)->where('token', $token)->firstOrFail();
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'token';
    }
}
