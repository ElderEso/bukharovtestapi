<?php

namespace App\Http\Controllers;

use App\Token;
use Illuminate\Http\Request;

class TokenController extends Controller
{

    /**
     * @return Token[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Token::all();
    }

    /**
     * @param Token $token
     * @return Token
     */
    public function show(Token $token)
    {
        return $token;
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
//       создаем объект и возвращаем созданную координату
        $token = Token::create(array_merge($request->all()));
        return response()->json($token, 201);
    }

    /**
     * @param Request $request
     * @param Token $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Token $token)
    {
        $token->update($request->all());

        return response()->json($token, 200);
    }

    /**
     * @param Token $token
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Token $token)
    {
        $token->delete();

        return response()->json(null, 204);
    }

    /**
     * метод для получения координат по токену
     * @param Token $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCoordinates(Token $token)
    {
        $coordinates = $token->coordinates->toArray();
        return response()->json($coordinates, 200);
    }
}
