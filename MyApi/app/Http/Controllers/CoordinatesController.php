<?php

namespace App\Http\Controllers;

use App\Coordinate;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CoordinatesController extends Controller
{

    /**
     * Get a validator for an incoming request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'token' => 'required|string|max:255|exists:tokens',
            'value_x' => 'required|numeric',
            'value_y' => 'required|numeric',
            'screen_height' => 'required|numeric',
            'screen_width' => 'required|numeric',
            'specific_url' => 'required|string',
        ]);
    }

    /**
     * @return Coordinate[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Coordinate::all();
    }

    /**
     * @param Coordinate $coordinate
     * @return Coordinate
     */
    public function show(Coordinate $coordinate)
    {
        return $coordinate;
    }

    /**
     * метод для сохраниния поступающих координат
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
//        валидируем данные на входе (эксепшен в хэндлере)
        $validator = $this->validator($request->all())->validate();
//      если все окей, то получаем токен по имени
        $token = Token::getTokenByName($request->input('token'));
//       создаем объект и возвращаем созданную координату
        $coordinate = Coordinate::create(array_merge($request->except('token'), ['token_id' => $token->id]));
        return response()->json($coordinate, 201);
    }

    /**
     * @param Request $request
     * @param Coordinate $coordinate
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Coordinate $coordinate)
    {
        $coordinate->update($request->all());

        return response()->json($coordinate, 200);
    }

    /**
     * @param Coordinate $coordinate
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Coordinate $coordinate)
    {
        $coordinate->delete();

        return response()->json(null, 204);
    }


}
