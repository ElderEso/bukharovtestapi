<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//роуты для координат
Route::get('coordinates', 'CoordinatesController@index');

Route::get('coordinates/{coordinate}', 'CoordinatesController@show');

Route::post('coordinates','CoordinatesController@store');

Route::put('coordinates/{coordinate}','CoordinatesController@update');

Route::delete('coordinates/{coordinate}', 'CoordinatesController@delete');

//роуты для токена
Route::get('tokens', 'TokenController@index');

Route::get('token/{token}', 'TokenController@show');

Route::post('tokens','TokenController@store');

Route::put('tokens/{token}','TokenController@update');

Route::delete('tokens/{token}', 'TokenController@delete');

Route::get('token/{token}/coordinates', 'TokenController@getCoordinates');